---
title: Contact
layout: layouts/page.njk
permalink: /contact/
eleventyNavigation:
  key: Contact
  order: 3
---

Deze website wordt onderhouden door <a class="text-blue-600" href="/vereniging/via">via</a>.

De code van deze website is publiek en <a class="text-blue-600" href="https://gitlab.com/studieverenigingvia/wiso/wisoweb">aan te passen op GitLab.</a>

Voor algemene vragen of opmerkingen kunt u terecht bij de desbetreffende vereniging.
Bij technische vragen over de website kunt u contact opnemen met <a class="text-blue-600" href="mailto:ict@svia.nl">ict@svia.nl</a>.
