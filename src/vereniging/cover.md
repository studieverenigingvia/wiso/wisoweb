---
layout: layouts/association.njk
title: Cover
hidetitle: true
location: Groningen
domain: KI en informatica
website: https://www.svcover.nl/
email: board@svcover.nl
logo: /img/cover.png
lat: 53.2217873
long: 6.4956536
---

Cover is de studievereniging voor Kunstmatige Intelligentie en Informatica aan
de Rijksuniversiteit Groningen. De vereniging telt ongeveer 950 leden.

Naast de bachelors Artificial Intelligence en Computing Science hebben wij
studenten van de masters Computing Science, Computational Cognitive Science en
Artificial Intelligence.

Als studievereniging organiseren wij naast een scala aan
gezelligheidsactiviteiten ook studiegerelateerde activiteiten die de studenten
helpen een beeld te krijgen van het bedrijfs- en onderzoeksleven. Dit doen wij
door middel van het organiseren van lezingen, bedrijfsbezoeken, binnen- en
buitenlandse excursies, en een jaarlijks symposium. Verder organiseren wij
studieondersteunende activiteiten en publiceren wij jaarlijks de ledenalmanak.
