---
layout: layouts/association.njk
title: Thalia
hidetitle: true
location: Nijmegen
domain: Informatica
website: https://thalia.nu/
email: info@thalia.nu
logo: /img/thalia.svg
lat: 51.8253465
long: 5.866497
---

Thalia is de studievereniging voor de computing science en information sciences
studenten van nijmegen. Ons primaire doel is de studenten onderling, maar ook
met de docenten in contact te brengen!

Met meer dan 600 leden is het gezellig druk bij ons, en er worden dan ook tal
van activiteiten georganiseerd. Veel van deze activiteiten zijn erg ontspannend,
maar ook de nodige educatieve evenementen mogen niet ontbreken. Denk hierbij aan
dingen als borrels, groepsactiviteiten in buurt en workshops. Meer informatie
over wat er precies gaande is is te vinden op onze website.

Tot slot organiseren we elk jaar de volgende grote activiteiten: een studietrip
naar het buitenland, een toffe introweek, een thalia weekend en een symposium.
