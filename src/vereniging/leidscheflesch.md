---
layout: layouts/association.njk
title: De Leidsche Flesch
location: Leiden
domain: Natuurkunde, Sterrenkunde, Wiskunde, Informatica
website: https://www.deleidscheflesch.nl/
email: bestuur@deleidscheflesch.nl
logo: /img/leidscheflesch.svg
lat: 52.1699413
long: 4.4546923
---

De Leidsche Flesch is de studievereniging voor Natuurkunde, Sterrenkunde,
Wiskunde en Informatica in Leiden. De voornaamste doelen van De Leidsche Flesch
zijn het bijdragen aan het contact tussen haar leden en de wetenschap en het
bijdragen aan het contact tussen de leden onderling. Deze dingen probeert ze te
realiseren door een breed scala aan activiteiten te organiseren, zoals
(lunch)lezingen, excursies, studiereizen, maar ook borrels, feesten, pubquizzen
en allerlei andere dingen. Verder laten we de studenten alvast kennismaken met
wat er allemaal mogelijk is na hun studie: behalve oriëntatie op mogelijkheden
om door te gaan in het onderzoek laten we de leden ook kennismaken met het
bedrijfsleven, d.m.v. bijvoorbeeld bedrijfslezingen of inhousedagen. Ten slotte
bieden we twee dingen waar je dagelijks veel plezier aan beleeft: er is elke dag
gratis koffie, thee en limonade in onze verenigingskamer en twee keer per jaar
kunnen leden via ons tegen een zeer voordelige prijs op een makkelijke manier
hun studieboeken bestellen.