---
layout: layouts/association.njk
title: NSA
hidetitle: true
location: Amsterdam
domain: Wiskunde, Natuurkunde, Sterrenkunde
website: https://www.nsaweb.nl/
email: bestuur@nsaweb.nl
logo: /img/nsa.png
lat: 52.354431
long: 4.956300
offset: -1em
---

De Natuurwetenschappelijke Studievereniging Amsterdam is de studievereniging
voor studenten natuurkunde, sterrenkunde, wiskunde en statistiek aan de UvA.

De NSA maakt de studie makkelijker en leuker. Leden krijgen bijvoorbeeld korting
op boeken en er is een online tentamenbank. Ook zijn er studiegerelateerde
activiteiten zoals lezingen en excursies.

Maar studeren is natuurlijk niet alleen maar met serieuze dingen bezig zijn. De
NSA organiseert regelmatig borrels en filmavonden en is ook nauw betrokken bij
grotere activiteiten in samenwerking met andere studieverenigingen. Ieder jaar
is er bijvoorbeeld het bètafeest, voor alle bètastudenten, en een buitenlandse
reis. Verder organiseert de NSA ieder jaar het introductieweekend voor haar
eerstejaars, samen met de ACD, de studievereniging voor scheikunde en bio-exact.


