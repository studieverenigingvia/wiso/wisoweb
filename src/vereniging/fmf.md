---
layout: layouts/association.njk
title: FMF
hidetitle: true
location: Groningen
domain: Wiskunde, Natuurkunde, Sterrenkunde
website: https://www.fmf.nl/
email: bestuur@fmf.nl
logo: /img/fmf.png
lat: 53.2217873
long: 6.4956536
offset: 1em
---

The FMF is the study association for (applied) physics, (applied) mathematics,
all consecutive master's programmes at the University of Groningen. We also
accept students of artificial intelligence, computer science, and astronomy as
members. We provide our over 800 members with lots of study and social
amenities.
