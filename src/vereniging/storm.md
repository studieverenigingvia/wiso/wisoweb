---
layout: layouts/association.njk
title: STORM
hidetitle: true
location: Amsterdam
domain: Wiskunde en informatica
website: https://www.storm.vu/
email: bestuur@storm.vu
logo: /img/storm.png
lat: 52.3328313
long: 4.8646983
offset: 1em
---

STORM is de studievereniging voor studenten van de divisie Wiskunde en
Informatica van de Faculteit Exacte Wetenschappen aan de VU te Amsterdam. STORM
stelt zich ten doel om de studenten te helpen om hun studietijd door te komen.
STORM houdt hiervoor een tentamenarchief bij, dat gedeeltelijk online staat op
de uitgebreide website. Daarnaast verzorgt STORM de verkoop van de boeken van
vakken welke aan onze Divisie gegeven worden. Ook onderhoudt STORM goede
contacten met studenten en docenten met betrekking tot de kwaliteit van het
onderwijs.

Daarnaast is er natuurlijk ook ruimte voor ontspanning tussen de colleges door.
Hiertoe organiseert STORM borrels en activiteiten, zoals een kerstdiner of een
koninginnedagboottocht. Bovendien is men momenteel bezig een studiereis te
organiseren. Natuurlijk is het ook mogelijk om in onze gezelligheidsruimte een
biertje te drinken of een spelletje te spelen.
