---
layout: layouts/association.njk
title: <b>via</b>
hidetitle: true
location: Amsterdam
domain: Informatica
website: https://svia.nl/
email: bestuur@svia.nl
logo: /img/via.svg
lat: 52.354431
long: 4.956300
---

**via** (Vereniging Informatiewetenschappen Amsterdam) is de studievereniging
voor studenten Informatica, Informatiekunde en Kunstmatige Intelligentie aan de
Universiteit van Amsterdam, inclusief studenten van de masters AI, Grid
Computing, Information studies, Software engineering en Systeem en netwerk
beheer.

**via** houdt zich bezig met de kwaliteit van het onderwijs en verzorgt de
verkoop van boeken en syllabi. Verder verzorgt zij presentaties, excursies naar
bedrijven, feesten, weekends, borrels, filmmiddagen, geeft een eigen blad uit en
onderhoudt haar website. Dit alles om het studeren te bevorderen en
veraangenamen voor haar leden.
