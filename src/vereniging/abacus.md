---
layout: layouts/association.njk
title: Abacus
location: Twente
domain: Technische Wiskunde
website: https://www.abacus.utwente.nl
email: bestuur@abacus.utwente.nl
logo: /img/abacus.png
lat: 52.232950
long: 6.586830
---

Abacus is de studievereniging van Technische Wiskunde in Twente. Met haar 664
leden is het ook de grootste wiskundige studievereniging van Nederland. De naam
'Abacus' komt van het Chinese telraam met dezelfde naam. Soms kom je er nog
eentje tegen in Nederland, vooral in oosterse winkeltjes of restaurants. Het is
verbazingwekkend om te zien hoe snel hier mee te rekenen is, en ook wat hier mee
te berekenen is. Zelfs worteltrekken is mogelijk met de Abacus, probeer dat maar
eens met een gewoon telraam!

Ten grondslag aan Abacus liggen twee doelstellingen. Ten eerste is dat het
promoten van de wiskunde en ten tweede het verbeteren van het contact tussen
wiskundigen, bijvoorbeeld studenten en docenten, maar ook wiskundigen van buiten
Twente.

Om deze doelstellingen te bereiken organiseert Abacus allerlei activiteiten.
Voorbeelden hiervan zijn lezingen, excursies, symposia en studiereizen. Dit zijn
de "serieuze" activiteiten, meestal georganiseerd voor en door studenten, om de
simpele reden dat ze eenzelfde interesse delen: wiskunde. Naast deze serieuze
activiteiten organiseert Abacus ook regelmatig activiteiten die minder met
wiskunde te maken lijken te hebben, zoals bijvoorbeeld borrels,
spelletjesavonden, een gala en feesten. Toch blijf je met deze activiteiten
onder wiskundigen en zal dit ook zo nu en dan naar voren komen. Een voorbeeld
hiervan is de kansberekening tijdens pokeravonden.

In samenwerking met de opleiding verzorgt Abacus elk jaar de introductie van AM
(een driedaags kamp), speciale borrels en propedeuse- en
bachelordiploma-uitreikingen. Ook is er elk jaar de ouderdag, georganiseerd door
de eerstejaarscommissie, waar ouders van eerstejaarsstudenten de kans krijgen om
kennis te maken met Abacus, AM en het studentenleven.

Aangezien een relatief groot deel van de leden medewerker is, probeert Abacus
ook deze leden tegemoet te komen. Activiteiten zoals een ‘Mathematisch Café’ en
‘Onderzoeker Onderzocht’ zijn interessant voor zowel studenten als medewerkers.
Zo kan je een docent ook nog eens op een informele manier tegenkomen. Zes keer
per jaar maakt Abacus, in samenwerking met de opleiding een blaadje: de
‘Ideaal!’. Hierin worden alle leden op de hoogte gehouden van alles wat er
binnen Abacus en de opleiding gebeurt.

Tenslotte verkoopt Abacus ook nog allerlei dingen, waarvan de boeken de
belangrijkste zijn. Abacus hoeft geen winst te maken en dus kan ze de boeken
veel goedkoper verkopen. Meestal heb je je lidmaatschap (€8,50 per jaar) er na
één keer boeken kopen al uit.

Ben je geïnteresseerd? Wil je nog wat meer weten? Kom een keertje langs in de
abacuskamer, er zijn daar genoeg mensen je vragen te beantwoorden. Een keertje
bellen of een e-mail sturen mag natuurlijk ook. We hopen je snel te mogen
begroeten als AM-student en Abacuslid!