---
layout: layouts/association.njk
title: GEWIS
hidetitle: true
location: Eindhoven
domain: Wiskunde en informatica
website: https://www.gewis.nl/
email: bestuur@gewis.nl
logo: /img/gewis.svg
lat: 51.4474291
long: 5.4851659
---

GEWIS is an abbreviation of GEmeenschap van Wiskunde en Informatica Studenten
(meaning: Community of Mathematics and Computer Science Students) and was
founded on June 28, 1982. GEWIS is a study association for students who study at
the department Mathematics and Computer Science at Eindhoven University of
Technology. As a study association we try to organize all kinds of fun and
interesting activities for our members. We have lectures, symposia, company
visits, personal development trainings, the occasional study trip abroad, and so
on. These study-oriented activities not only teach you something, they are also
very fun and you get a different view of your studies and your (later) work.

In addition to study-oriented activities, GEWIS also organizes a waterfall of
so-called "social activities". GEWIS members are therefore quite good at pool,
darts, archery (etc.), regularly watch a video and of course come for drinks
every Thursday. In addition, occasionally weekends are arranged. For example, we
go sailing for a weekend, we run with a team in the Batavieren race, we have a
walking weekend and we also have a hitchhiking weekend.

Of course, we have also an association magazine ("Supremum", appears 3 times a
year) and you can visit us in the association room, 'Het Dakterras', during the
break or after lecture time for a chat with your fellow students or a game of
table football or pinball. Or to buy a book - for members at a discount!

