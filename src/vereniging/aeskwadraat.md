---
layout: layouts/association.njk
title: A-Eskwadraat
hidetitle: true
location: Utrecht
domain:  Gametechnologie, Informatica, Informatiekunde, Natuurkunde, Wiskunde
website: https://www.a-eskwadraat.nl
email: bestuur@a-eskwadraat.nl
logo: /img/awskwadraat.png
lat: 52.091259
long: 5.122750
offset: 1em
---

Eskwadraat is de studievereniging voor studenten Wiskunde, Natuurkunde,
Informatica, Informatiekunde en alle GSNS-studenten van de Universiteit Utrecht.
Met ruim 2000 leden zijn wij de grootste beta-studievereniging van Nederland.
Door zoveel studies en mensen samen te brengen in één vereniging, heeft
A–Eskwadraat een hoge diversiteit. Er worden veel verschillende activiteiten
georganiseerd, voor alle studenten, uiteenlopend van de introductie tot
arbeidsmarktoriëntatie-gerelateerde activiteiten. Natuurlijk beschikt ook
A–Eskwadraat over alle commissies en activiteiten die een grote studievereniging
eigenlijk in zijn activiteitenwaaier zou moeten hebben: een studiereis, een
eerstejaarsweekend, een almanak, een symposium, ... en ga zo maar door.

A–Eskwadraat heeft een rijke historie: de eerste tekenen van een
beta-studievereniging in Utrecht dateren van 4 mei 1928, met de oprichting van
S-kwadraat, een studiegezelschap voor na-candidaten in de fysica aan de
Universiteit Utrecht. Waar de letters S-S voor staan is al in de vijftiger jaren
vergeten, volgens een van de leden van het eerste uur staat het voor Samen
Slimmer, andere mogelijkheden die genoemd worden zijn: Samen Sterker en Samen
Studeren. Ook de niet-candidaten verenigen zich: in A-E (A tot E). De letters
verwijzen naar vijf mogelijkheden van het vakkenpakket binnen het
candidaatsexamen. Deze vereniging wordt opgericht op 17 mei 1934. S-kwadraat
komt in de jaren zestig voor de tweede keer in zijn bestaan in de problemen. Er
zijn steeds minder mensen te vinden die lid willen worden van de vereniging en
die hun contributie willen betalen. Ook bij A-E ziet het er lang niet
rooskleurig uit en op 10 februari 1971 is de fusie een feit. Als werknaam wordt
A–Eskwadraat gekozen, men gaat op zoek naar een beter alternatief. Dat wordt
echter nooit gevonden en zo is A–Eskwadraat nog steeds de naam van onze mooie
vereniging en zal dat nog lang zo blijven!

A-Eskwadraat is inmiddels een begrip geworden in studieverenigingen-land en ook
op onze faculteiten is veel waardering voor onze activiteiten. Wij zijn er dan
ook van overtuigd dat, ook in de aanstaande samenvoeging van de beta-faculteiten
in Utrecht tot \'e\'en beta-federatie, de plek van A-Eskwadraat gewaarborgd zal
zijn, het is immers niet voor niets dat er nu al nagedacht wordt over het
eeuw-feest in 2071!
