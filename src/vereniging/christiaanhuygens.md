---
layout: layouts/association.njk
title: W.I.S.V. Christiaan Huygens
hidetitle: true
location: Delft
domain: Wiskunde en informatica
website: https://ch.tudelft.nl/
email: bestuur@ch.tudelft.nl
logo: /img/christiaanhuygens.png
lat: 52.011902
long: 4.360260
offset: 1em
---

De Wiskunde en Informatica Studievereniging "Christiaan Huygens" (CH) is
opgericht op 6 maart 1957. Van oorsprong was CH alleen een studievereniging voor
Technische Wiskunde, maar 25 jaar na de oprichting is ook Technische Informatica
onderdeel geworden van onze studievereniging. Inmiddels is CH uitgegroeid tot
ongeveer 1500 leden, waarvan 150-200 actief.

CH zet zich in voor studenten wiskunde en informatica aan de TU Delft. Haar
doelstellingen zijn als volgt gedefinieerd:

  - Het behartigen van de studiebelangen van studerenden in de Wiskunde en
    Informatica aan de Technische Universiteit Delft;
  - Het scheppen van een band tussen deze studerenden;
  - Het scheppen van een band tussen de alumni en de studerenden van de
    bovengenoemde studierichtingen.

CH behartigt de studiebelangen door middel van verschillende activiteiten, zoals
het verkopen van studieboeken en het beschikbaar stellen van oude tentamens. Ook
houdt CH het onderwijs in de gaten en probeert eventuele problemen op te lossen
in samenwerking met de docenten en studenten. Daarnaast organiseert CH allerlei
activiteiten om de kennis van studenten over hun vakgebieden te vergroten, zoals
lezingen, excursies en symposia. Om het jaar wordt er een studiereis naar het
buitenland georganiseerd, in de overige jaren kunnen CH leden bedrijven in
Nederland of Europa leren kennen tijdens de bedrijvenreis. Zo kunnen onze leden
zich alvast oriënteren op hun carriëre mogelijkheden. Daarnaast doet CH mee aan
de organisatie van De Delftse Bedrijvendagen, een carriërebeurs voor studenten
aan de TU Delft.

Voor CH is de band tussen studenten zeer belangrijk. Om deze band te verbeteren
organiseert CH veel activiteiten, zoals gala's, feesten, lunches,
programmeerwedstrijden, LAN-parties, een eerstejaarsweekend en een
ouderejaarsweekend. Deze activiteiten worden grotendeels neergezet door de vele
commissies die CH rijk is. Ook zijn er commissies om een jaarboek uit te
brengen, het verenigingsblad MaCHazine te publiceren, en contact met alumni te
onderhouden. Voor een overzicht van alle commissies kunt u terecht op onze
website.
