---
layout: layouts/association.njk
title: Inter-<i>Actief</i>
location: Enschede
domain: Informatica
website: https://www.inter-actief.utwente.nl/
email: contact@inter-actief.net
logo: /img/interactief.png
lat: 52.232950
long: 6.586830
offset: 1em
---

I.C.T.S.V. Inter-Actief is the study association for the studies Technical
Computer Science, Business Information Technology and the corresponding masters
at the University of Twente. Inter-Actief was founded on the 12th of March 1981
and has approximately 1400 members, making her the largest ICT-study association
in the Netherlands.

Inter-Actief facilitates several services to make student life more pleasant.
You can always come to Inter-Actief for study-related complaints and other
problems, where Inter-Actief will try to support you as much as possible. We
also keep you informed of what is happening in our studies themselves. Next to
all study-related aspects, Inter-Actief also facilitates a lot of fun. We have
weekly drinks on Tuesday afternoon and occasionally throw a party. Besides that,
we host all kinds of activities such as laser gaming, symposia, workshops,
barbecues, LAN parties and many more. During these activities, you can get to
know your fellow students better while not having to think about studying the
whole time. Next to the daily activities, Inter-Actief organises several trips.
Each year Inter-Actief goes on a skiing trip, where we enjoy the snow for a full
week for a student fee. We also visit a big European city once a year, where we
visit a local university and a couple innovative local businesses. Our largest
journey is the biennial study tour, where we visit an interesting country far
abroad and have many company- and education-related activities.
