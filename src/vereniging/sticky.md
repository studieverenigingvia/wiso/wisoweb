---
layout: layouts/association.njk
title: Sticky
hidetitle: true
location: Utrecht
domain: Informatica, Informatiekunde
website: https://svsticky.nl/
email: bestuur@svsticky.nl
logo: /img/sticky.svg
lat: 52.091259
long: 5.122750
---

Sticky is de studievereniging speciaal voor informatica en informatiekunde
studenten uit Utrecht. Sticky is een jonge vereniging welke is opgericht in
2006, als extra vereniging naast A-Eskwadraat (die naast informatici en
informatiekundigen, ook wis- en natuurkundigen uit Utrecht dient).

Sticky heeft als doel om het contact tussen studenten onderling te verbeteren,
om studenten (meer) te betrekken bij het onderwijs en medezeggenschap, en om
studenten (beter) kennis te laten maken met het bedrijfsleven. De activiteiten
die Sticky organiseert zijn dan ook verpreid over deze drie 'pijlers'; zo zijn
er bijvoorbeeld gezelligheidsactiviteiten zoals borrels en uitstapjes, onderwijs
gerelateerde activiteiten zoals discussiemiddagen, en worden lezingen en
inhousedagen bij bedrijven georganiseerd.
