---
layout: layouts/association.njk
title: DESDA
location: Nijmegen
domain: Wiskunde
website: https://www.desda.org/
email: info@desda.org
logo: /img/desda.png
lat: 51.8253465
long: 5.866497
offset: 1em
---

DESDA (Dan en slechts dan als) is de wiskunde-studievereniging van de Radboud
Universiteit Nijmegen. DESDA is een kleine (ca. 250 leden, inclusief de meeste
docenten) maar zeer actieve vereniging. Naast het vijfkoppige bestuur bestaat de
vereniging uit een aantal vaste commissies die ervoor zorgen dat er altijd iets
te doen is. Hieronder staan de vier grootste commissies.

- Springer is de sport- en spel-commissie. Door het jaar heen worden
  activiteiten georganiseerd als de sportdag, een dropping, het bowlingtoernooi
  en het pooltoernooi

- Gardner organiseert lezingen over diverse wiskundige onderwerpen, excursies
  naar bedrijven waar wiskundigen terecht kunnen komen, ouderdagen etc.

- Brouwer is de commissie die de verschillende borrels door het jaar heen
  verzorgt. Denk hierbij aan borrels bij bul- en propedeuse-uitreikingen, met
  sinterklaas, kerst of gewoon zomaar...

- Bartjens verzorgt het verenigingsblad, de Volgens Bartjens, dat vier maal per
  jaar uitkomt, en het Smoelenboek.

Buiten deze vaste commissies heeft DESDA een aantal gelegenheidscommissies. Zo
wordt er ieder jaar het DESDA-weekend georganiseerd. In dit weekend worden over
drie dagen verschillende sport- en spelactiviteiten georganiseerd.

Al met al is DESDA een relatief kleine vereniging, maar de betrokkenheid van
haar leden is groot en dat houdt de vereniging dynamisch en vernieuwend.
