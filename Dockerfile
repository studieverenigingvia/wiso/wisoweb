FROM node:18 as static-build

WORKDIR /app
COPY package*.json /app/
RUN npm ci
COPY ./ /app
RUN npm run build

FROM nginx:alpine

COPY --from=static-build /app/_site /usr/share/nginx/html
