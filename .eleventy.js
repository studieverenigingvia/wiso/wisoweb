const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const markdownIt = require("markdown-it");

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(eleventyNavigationPlugin);
  eleventyConfig.addPassthroughCopy('src/img');
  eleventyConfig.addPassthroughCopy({ "src/img/favicon": "/" });

  let options = {
    html: true,
    breaks: false,
    linkify: true
  };

  eleventyConfig.setLibrary("md", markdownIt(options));

  const {
    DateTime
  } = require("luxon");

  // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
  eleventyConfig.addFilter('htmlDateString', (dateObj) => {
    return DateTime.fromJSDate(dateObj, {
      zone: 'utc'
    }).toFormat('yy-MM-dd');
  });

  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj, {
      zone: 'utc'
    }).toFormat("dd-MM-yy");
  });

  eleventyConfig.addNunjucksShortcode("associationmap", function (lat, long, offset = '') {
    if (!lat || !long) return '';

    // estimated svg coordinates of nederland.svg:
    // left-right: 3.082 - 7.311 (4,229)
    // top-bottom: 53.622 - 50.726 (2,896)

    let left = ((parseFloat(long) - 3.082) / 4.4);
    let top = -((parseFloat(lat) - 53.549) / 2.896);

    let topcss = offset ? `calc(${top * 100}% + ${offset})` : `${top * 100}%`;
    return `left: ${left * 100}%; top: ${topcss};`
  });

  eleventyConfig.addCollection("associationsSorted", function (collectionApi) {
    return collectionApi.getFilteredByTags("associations")
      .sort((a, b) => a.data.title.replace('<b>', '').toLowerCase().localeCompare(b.data.title.replace('<b>', '').toLowerCase()));
  });

  return {
    dir: {input: 'src', output: '_site'}
  };
};
